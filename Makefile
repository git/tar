TARVER=1.29b
DEBREL=2
PVETARVER=${TARVER}+pve.1

# also update changelog

TARSRC=tar_${TARVER}.orig.tar.xz
TARDEBSRC=tar_${TARVER}-${DEBREL}.debian.tar.xz
TARDIR=tar-${TARVER}

ARCH:=$(shell dpkg-architecture -qDEB_BUILD_ARCH)
GITVERSION:=$(shell cat .git/refs/heads/master)
SOURCETXT="git clone git://git.proxmox.com/git/tar.git\\ngit checkout ${GITVERSION}"

DEB=tar_${PVETARVER}_${ARCH}.deb

all: deb

.PHONY: dinstall
dinstall: ${DEB}
	dpkg -i ${DEB}

.PHONY: prepare
prepare: $(TARDIR)
$(TARDIR): $(TARSRC) $(TARDEBSRC)
	rm -rf ${TARDIR}
	tar xf ${TARSRC}
	tar -C $(TARDIR) -xf $(TARDEBSRC)
	echo "${SOURCETXT}" > ${TARDIR}/debian/SOURCE
	echo "debian/SOURCE" >>${TARDIR}/debian/docs
	mv ${TARDIR}/debian/changelog ${TARDIR}/debian/changelog.org
	cat changelog ${TARDIR}/debian/changelog.org >${TARDIR}/debian/changelog
	cd ${TARDIR} && patch -p1 -i ../sparse-unicode.patch

.PHONY: deb
deb: $(DEB)
$(DEB): $(TARDIR)
	cd ${TARDIR} && dpkg-buildpackage -b --no-sign

.PHONY: upload
upload: $(DEB)
	tar cf - ${DEB} | ssh -X repoman@repo.proxmox.com -- upload --product pve --dist stretch

.PHONY: clean
clean:
	rm -rf *~ *_${ARCH}.deb *_all.deb *_${ARCH}.udeb *.changes *.buildinfo *.dsc ${TARDIR}
